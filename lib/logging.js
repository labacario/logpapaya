"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
Object.defineProperty(exports, "__esModule", { value: true });
const winston = require("winston");
const logdna = require("logdna");
const options = {
    app: "default-app",
    env: "default-env",
    index_meta: true,
    handleExceptions: true,
};
const defaultLevels = { error: 0, warn: 1, info: 2, verbose: 3, debug: 4, silly: 5 };
const prodLevels = { error: 0, warn: 1, info: 2 };
class Logging {
    /**
     * constructor. Create and configure logging params
     *
     * @constructor
     */
    constructor() {
    }
    init(config, environnement) {
        console.log(Object.assign({}, options, config, { hostname: environnement }));
        winston.add(logdna.WinstonTransport, Object.assign({}, options, config, { hostname: environnement }));
        if (environnement == "production") {
            winston.setLevels(prodLevels);
        }
        else {
            winston.setLevels(defaultLevels);
        }
    }
    error(message) {
        winston.level = 'error';
        winston.log('error', message);
    }
    warn(message) {
        winston.level = 'warn';
        winston.log('warn', message);
    }
    info(message) {
        winston.level = 'info';
        winston.log('info', message);
    }
    verbose(message) {
        winston.level = 'verbose';
        winston.log('verbose', message);
    }
    debug(message) {
        winston.level = 'debug';
        winston.log('debug', message);
    }
    silly(message) {
        winston.level = 'silly';
        winston.log('silly', message);
    }
}
exports.logging = new Logging();

//# sourceMappingURL=logging.js.map
